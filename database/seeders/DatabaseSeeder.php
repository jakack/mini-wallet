<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        if(\DB::table('users')->count() == 0){

            \DB::table('users')->insert([

                [
                    'id' => "ea0212d3-abd6-406f-8c67-868e814a2436",
                    'name' => 'customer 1',
                    'email' => 'customer1@app.com',
                    'role' => 'customer',
                    'password' => bcrypt('password'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => "ea0212d3-abd6-406f-8c67-338e814a2416",
                    'name' => 'customer 2',
                    'email' => 'customer2@app.com',
                    'role' => 'customer',
                    'password' => bcrypt('password'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => "ea0212d3-abd6-406f-8c67-822e814a2411",
                    'name' => 'customer 2',
                    'email' => 'customer3@app.com',
                    'role' => 'customer',
                    'password' => bcrypt('password'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]

            ]);

        } else { echo "\e[31mTable is not empty, therefore NOT "; }

    }
}
