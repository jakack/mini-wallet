## How to run project Mini Wallet

- 1.Clone this repo to your local
- 2.Open solution with IDE (Visual Studio is recommended)
- 3.Update file .env Update DefaultConnection based on your local mysql database credential.
- 4.Open cmd and run this command :
    - php artisan migrate
    - php artisan db:seed
    - php artisan serve
- 5.You are ready to go!

## Regsiter user Method Post

- **http://127.0.0.1:8000/api/v1/register

form data
- ** name
- ** email
- ** password
- ** c_password

## Get all user Method Get

- **http://127.0.0.1:8000/api/v1/users

## Detail Documentation API
- **https://documenter.getpostman.com/view/6442940/2s84DrPMYT
