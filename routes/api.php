<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\WalletController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::controller(RegisterController::class)->group(function(){
    Route::post('v1/register', 'register');
    Route::post('v1/login', 'login');
    Route::post('v1/init', 'init_wallet');
    Route::get('v1/users', 'users');
});

Route::middleware('auth:sanctum')->group( function () {

    Route::controller(WalletController::class)->group(function(){

        Route::get('v1/wallet', 'get_wallet');
        Route::post('v1/wallet/deposits', 'deposit');
        Route::post('v1/wallet/withdrawals', 'withdrawals');
        Route::post('v1/wallet', 'enable');
        Route::patch('v1/wallet', 'disable');
    });

});

