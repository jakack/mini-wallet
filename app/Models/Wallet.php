<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
class Wallet extends Model
{
    use HasFactory, Uuids;

    protected $table = 'wallet';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'customer_xid','is_disabled', 'description','created_by', 'updated_by', 'created_at', 'updated_at'
    ];
}
