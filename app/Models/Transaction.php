<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
class Transaction extends Model
{
    use HasFactory, Uuids;
    protected $table = 'transaction';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'walled_id', 'description', 'amount', 'status','created_by', 'updated_by', 'created_at', 'updated_at', 'type', 'reference_id'
    ];
}
