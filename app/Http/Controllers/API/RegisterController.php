<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Resources\UsersResource;

class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors());
            }

            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['token'] =  $user->createToken('MyApp')->plainTextToken;
            $success['name'] =  $user->name;

            return $this->sendResponse($success, 'User register successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }

    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->plainTextToken;
            $success['name'] =  $user->name;

            return $this->sendResponse($success, 'User login successfully.');
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }

    public function init_wallet(Request $request)
    {
        $customer_xid = $request->customer_xid;
        if(Auth::loginUsingId($customer_xid)){
            //create
            $user = Auth::user();
            $token =  $user->createToken('MyApp')->plainTextToken;
            try {
                //code...
                Wallet::create([
                    'customer_xid' => $customer_xid,
                    'is_disabled' => true
                ]);
                return $this->sendResponse(array('token' => $token), 'Init Wallet successfully.');
            } catch (\Throwable $th) {
                //throw $th;
                return $this->sendResponse(array('token' => $token), 'Wallet Already Init.');
            }

        }else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }


        // return $this->sendResponse($wallet, 'Product created successfully.');
    }

    public function users(Request $request)
    {
        $users = User::all();
        if($users){
            return $this->sendResponse($users, 'All users.');
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }
}
