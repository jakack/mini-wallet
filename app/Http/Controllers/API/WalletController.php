<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Wallet;
use App\Models\Transaction;
use Validator;
use App\Http\Resources\WalletResource;

class WalletController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_wallet(Request $request)
    {
        $id = $request->user()->id;
        $wallet = Wallet::where('customer_xid', $id)->get();
        if ($wallet) {
            if ($wallet[0]->is_disabled == 0) {
                $deposits = Transaction::where('walled_id', $wallet[0]->id)->where('type', 'deposit')->sum('amount');
                $withdraws = Transaction::where('walled_id', $wallet[0]->id)->where('type', 'withdrawals')->sum('amount');
                $status = ($wallet[0]->is_disabled == 1)? "disabled" : "enabled";
                $balance = $deposits - $withdraws;
                return $this->sendResponse(array(
                    'id' => $wallet[0]->id,
                    'owned_by' => $wallet[0]->customer_xid,
                    'status' => $status,
                    "enabled_at" => $wallet[0]->updated_at,
                    'balance' => $balance
                ), 'Wallet Find successfully.');
            }else{
                return $this->sendError('Wallet disabled.', ['error'=>'Wallet disabled']);
            }

        }else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }

    }

    public function enable(Request $request)
    {
        $input = $request->all();

        $id = $request->user()->id;
        $wallet = Wallet::where('customer_xid', $id)->get();

        if ($wallet) {
            $updateWallet = Wallet::where("customer_xid", $id)->update(["is_disabled" => false]);
            $deposits = Transaction::where('walled_id', $wallet[0]->id)->where('type', 'deposit')->sum('amount');
            $withdraws = Transaction::where('walled_id', $wallet[0]->id)->where('type', 'withdrawals')->sum('amount');
            $status = ($wallet[0]->is_disabled == 1)? "disabled" : "enabled";
            $balance = $deposits - $withdraws;
            return $this->sendResponse(array(
                'id' => $wallet[0]->id,
                'owned_by' => $wallet[0]->customer_xid,
                'status' => $withdraws,
                "enabled_at" => $wallet[0]->updated_at,
                'balance' => $balance
            ), 'Wallet enable successfully.');
        }else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }

    public function disable(Request $request)
    {

        $id = $request->user()->id;
        $wallet = Wallet::where('customer_xid', $id)->get();

        if ($wallet) {
            $updateWallet = Wallet::where("customer_xid", $id)->update(["is_disabled" => true]);
            $deposits = Transaction::where('walled_id', $wallet[0]->id)->where('type', 'deposit')->sum('amount');
            $withdraws = Transaction::where('walled_id', $wallet[0]->id)->where('type', 'withdrawals')->sum('amount');
            $status = ($wallet[0]->is_disabled == 1)? "disabled" : "enabled";
            $balance = $deposits - $withdraws;
            return $this->sendResponse(array(
                'id' => $wallet[0]->id,
                'owned_by' => $wallet[0]->customer_xid,
                'status' => $withdraws,
                "enabled_at" => $wallet[0]->updated_at,
                'balance' => $balance
            ), 'Wallet disabled successfully.');
        }else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }

    }

    // Transaction
    public function deposit(Request $request)
    {
        try {
            $id = $request->user()->id;
            $wallet = Wallet::where('customer_xid', $id)->get();
            if ($wallet) {
                if ($wallet[0]->is_disabled == 0) {

                    $input = $request->all();
                    $validator = Validator::make($input, [
                        'amount' => 'required',
                        'reference_id'  => 'required',
                    ]);

                    if($validator->fails()){
                        return $this->sendError('Validation Error.', $validator->errors());
                    }
                    $transaction   =   Transaction::create(
                        [
                        'amount' => $request->amount,
                        'reference_id' => $request->reference_id,
                        'walled_id' => $wallet[0]['id'],
                        'type' => 'deposit',
                        'status' => 1,
                        'created_by' => $id,
                        'updated_by' => $id,
                    ]);

                    return $this->sendResponse(array(
                        'id' => $transaction->id,
                        'deposited_by' => $transaction->created_by,
                        'status' => "success",
                        'deposited_at' => $transaction->created_at,
                        'amount' => $transaction->amount,
                        'reference_id' => $transaction->reference_id,
                    ), 'Deposit Transaction created successfully.');

                }else{
                    return $this->sendError('Wallet disabled.', ['error'=>'Wallet disabled']);
                }

            }else{
                return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
            }
        } catch (\Throwable $th) {
            return $this->sendError('there is an error.', ['error'=>'Unauthorised']);
        }
    }
    //With draw
    public function withdrawals(Request $request)
    {
        try {
            $id = $request->user()->id;
            $wallet = Wallet::where('customer_xid', $id)->get();
                if ($wallet) {
                    if ($wallet[0]->is_disabled == 0) {
                        $input = $request->all();
                        $validator = Validator::make($input, [
                            'amount' => 'required',
                            'reference_id'  => 'required',
                        ]);

                        if($validator->fails()){
                            return $this->sendError('Validation Error.', $validator->errors());
                        }

                        $deposits = Transaction::where('walled_id', $wallet[0]->id)->where('type', 'deposit')->sum('amount');
                        $withdraws = Transaction::where('walled_id', $wallet[0]->id)->where('type', 'withdrawals')->sum('amount');
                        $status = ($wallet[0]->is_disabled == 1)? "disabled" : "enabled";
                        $balance = $deposits - $withdraws;
                        $balanceTotal = $balance - $request->amount;
                        if ($balanceTotal > 0) {
                            $transaction   =   Transaction::create(
                                [
                                'amount' => $request->amount,
                                'reference_id' => $request->reference_id,
                                'walled_id' => $wallet[0]['id'],
                                'type' => 'withdrawals',
                                'status' => 1,
                                'created_by' => $id,
                                'updated_by' => $id,
                            ]);
                            return $this->sendResponse(array(
                                'id' => $transaction->id,
                                'deposited_by' => $transaction->created_by,
                                'status' => "success",
                                'deposited_at' => $transaction->created_at,
                                'amount' => $transaction->amount,
                                'reference_id' => $transaction->reference_id,
                            ), 'Withdrawals Transaction created successfully.');
                    }else{
                        return $this->sendResponse("amount not enough", 'Withdrawals Transaction failed.');
                    }

                }else{
                    return $this->sendError('Wallet disabled.', ['error'=>'Wallet disabled']);
                }

            }else{
                return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
            }
        } catch (\Throwable $th) {
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }

    }

}
